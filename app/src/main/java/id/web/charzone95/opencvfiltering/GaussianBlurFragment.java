package id.web.charzone95.opencvfiltering;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GaussianBlurFragment extends Fragment {
    private static final String ARG_FILENAME = "filename";

    private String fileName;

    @BindView(R.id.imageViewHasil)
    ImageView imageViewHasil;

    Bitmap imageBitmap;

    @BindView(R.id.buttonOriginal)
    AppCompatButton buttonOriginal;

    @BindView(R.id.buttonProcess)
    AppCompatButton buttonProcess;

    @BindView(R.id.seekBarSize)
    AppCompatSeekBar seekBarSize;

    @BindView(R.id.textViewSize)
    TextView textViewSize;

    int sizeValue;

    Mat originalMat;
    Mat resultMat;

    Bitmap bmp32;
    Bitmap resultBitmap;


    public GaussianBlurFragment() {
        // Required empty public constructor
    }

    public static GaussianBlurFragment newInstance(String filename) {
        GaussianBlurFragment fragment = new GaussianBlurFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FILENAME, filename);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fileName = getArguments().getString(ARG_FILENAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gaussian_blur, container, false);
        ButterKnife.bind(this, view);

        //load scaled bitmap
        int targetWidth = 800;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, bmOptions);
        int imageWidth = bmOptions.outWidth;

        int scaleFactor = imageWidth/targetWidth;

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;


        imageBitmap = BitmapFactory.decodeFile(fileName, bmOptions);

        seekBarSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                sizeValue = seekBar.getProgress() * 2 + 1;
                textViewSize.setText(String.valueOf(sizeValue));

                processFilter();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        sizeValue = 0;
        seekBarSize.setMax(50);
        seekBarSize.setProgress(0);
        sizeValue = seekBarSize.getProgress() * 2 + 1;
        textViewSize.setText("1");


        seekBarSize.setEnabled(false);
        imageViewHasil.setImageBitmap(imageBitmap);

        return view;
    }

    @OnClick(R.id.buttonOriginal)
    public void buttonOriginalClick() {
        seekBarSize.setEnabled(false);

        imageViewHasil.setImageBitmap(imageBitmap);
    }

    @OnClick(R.id.buttonProcess)
    public void buttonProcessClick() {
        seekBarSize.setEnabled(true);

        processFilter();
    }

    private void processFilter() {
        originalMat = new Mat();
        bmp32 = imageBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, originalMat);

        resultMat = new Mat(originalMat.size(), originalMat.type());

        Imgproc.GaussianBlur(originalMat, resultMat, new Size(sizeValue, sizeValue), 0);

        resultBitmap = Bitmap.createBitmap(resultMat.cols(), resultMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(resultMat, resultBitmap);
        imageViewHasil.setImageBitmap(resultBitmap);

    }



}
