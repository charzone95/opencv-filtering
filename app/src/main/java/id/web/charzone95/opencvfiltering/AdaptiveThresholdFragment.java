package id.web.charzone95.opencvfiltering;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdaptiveThresholdFragment extends Fragment {
    private static final String ARG_FILENAME = "filename";

    private String fileName;

    @BindView(R.id.imageViewHasil)
    ImageView imageViewHasil;

    Bitmap imageBitmap;

    @BindView(R.id.buttonOriginal)
    AppCompatButton buttonOriginal;

    @BindView(R.id.buttonProcess)
    AppCompatButton buttonProcess;

    @BindView(R.id.seekBarBlockSize)
    AppCompatSeekBar seekBarBlockSize;

    @BindView(R.id.textViewValueBlockSize)
    TextView textViewValueBlockSize;

    @BindView(R.id.seekBarC)
    AppCompatSeekBar seekBarC;

    @BindView(R.id.textViewValueC)
    TextView textViewValueC;

    int blockSizeValue;
    int cValue;

    Mat originalMat;
    Mat grayMat;
    Mat resultMat;

    Bitmap bmp32;
    Bitmap resultBitmap;


    public AdaptiveThresholdFragment() {
        // Required empty public constructor
    }

    public static AdaptiveThresholdFragment newInstance(String filename) {
        AdaptiveThresholdFragment fragment = new AdaptiveThresholdFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FILENAME, filename);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fileName = getArguments().getString(ARG_FILENAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adaptive_threshold, container, false);
        ButterKnife.bind(this, view);

        //load scaled bitmap
        int targetWidth = 800;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, bmOptions);
        int imageWidth = bmOptions.outWidth;

        int scaleFactor = imageWidth/targetWidth;

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;


        imageBitmap = BitmapFactory.decodeFile(fileName, bmOptions);

        seekBarBlockSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                blockSizeValue = seekBar.getProgress() * 2 + 3;
                textViewValueBlockSize.setText(String.valueOf(blockSizeValue));

                processFilter();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        blockSizeValue = 0;
        seekBarBlockSize.setMax(20);
        seekBarBlockSize.setProgress(0);
        blockSizeValue = seekBarBlockSize.getProgress() * 2 + 3;

        seekBarC.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                cValue = seekBarC.getProgress() + 1;
                textViewValueC.setText(String.valueOf(cValue));

                processFilter();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        cValue = 0;
        seekBarC.setMax(99);
        seekBarC.setProgress(39);
        cValue = seekBarC.getProgress() + 1;



        seekBarBlockSize.setEnabled(false);
        seekBarC.setEnabled(false);
        imageViewHasil.setImageBitmap(imageBitmap);

        return view;
    }

    @OnClick(R.id.buttonOriginal)
    public void buttonOriginalClick() {
        seekBarBlockSize.setEnabled(false);
        seekBarC.setEnabled(false);

        imageViewHasil.setImageBitmap(imageBitmap);
    }

    @OnClick(R.id.buttonProcess)
    public void buttonProcessClick() {
        seekBarBlockSize.setEnabled(true);
        seekBarC.setEnabled(true);

        processFilter();
    }

    private void processFilter() {
        originalMat = new Mat();
        bmp32 = imageBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, originalMat);

        grayMat = new Mat(originalMat.size(), originalMat.type());
        Imgproc.cvtColor(originalMat, grayMat, Imgproc.COLOR_RGBA2GRAY);


        resultMat = new Mat(grayMat.size(), grayMat.type());

        Imgproc.adaptiveThreshold(grayMat, resultMat, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, blockSizeValue, cValue);

        resultBitmap = Bitmap.createBitmap(resultMat.cols(), resultMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(resultMat, resultBitmap);
        imageViewHasil.setImageBitmap(resultBitmap);

    }



}
