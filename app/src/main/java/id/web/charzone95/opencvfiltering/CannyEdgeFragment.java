package id.web.charzone95.opencvfiltering;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CannyEdgeFragment extends Fragment {
    private static final String ARG_FILENAME = "filename";

    private String fileName;

    @BindView(R.id.imageViewHasil)
    ImageView imageViewHasil;

    Bitmap imageBitmap;

    @BindView(R.id.buttonOriginal)
    AppCompatButton buttonOriginal;

    @BindView(R.id.buttonProcess)
    AppCompatButton buttonProcess;

    @BindView(R.id.seekBarThreshold1)
    AppCompatSeekBar seekBarThreshold1;

    @BindView(R.id.textViewThreshold1)
    TextView textViewThreshold1;

    @BindView(R.id.seekBarThreshold2)
    AppCompatSeekBar seekBarThreshold2;

    @BindView(R.id.textViewThreshold2)
    TextView textViewThreshold2;

    int valueThreshold1;
    int valueThreshold2;

    Mat originalMat;
    Mat grayMat;
    Mat resultMat;

    Bitmap bmp32;
    Bitmap resultBitmap;


    public CannyEdgeFragment() {
        // Required empty public constructor
    }

    public static CannyEdgeFragment newInstance(String filename) {
        CannyEdgeFragment fragment = new CannyEdgeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FILENAME, filename);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fileName = getArguments().getString(ARG_FILENAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_canny_edge, container, false);
        ButterKnife.bind(this, view);

        //load scaled bitmap
        int targetWidth = 800;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, bmOptions);
        int imageWidth = bmOptions.outWidth;

        int scaleFactor = imageWidth/targetWidth;

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;


        imageBitmap = BitmapFactory.decodeFile(fileName, bmOptions);

        seekBarThreshold1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                valueThreshold1 = seekBar.getProgress();
                textViewThreshold1.setText(String.valueOf(valueThreshold1));

                processFilter();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        valueThreshold1 = 0;
        seekBarThreshold1.setMax(255);
        seekBarThreshold1.setProgress(80);
        valueThreshold1 = seekBarThreshold1.getProgress();

        seekBarThreshold2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                valueThreshold2 = seekBarThreshold2.getProgress();
                textViewThreshold2.setText(String.valueOf(valueThreshold2));

                processFilter();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        valueThreshold2 = 0;
        seekBarThreshold2.setMax(255);
        seekBarThreshold2.setProgress(100);
        valueThreshold2 = seekBarThreshold2.getProgress();



        seekBarThreshold1.setEnabled(false);
        seekBarThreshold2.setEnabled(false);
        imageViewHasil.setImageBitmap(imageBitmap);

        return view;
    }

    @OnClick(R.id.buttonOriginal)
    public void buttonOriginalClick() {
        seekBarThreshold1.setEnabled(false);
        seekBarThreshold2.setEnabled(false);

        imageViewHasil.setImageBitmap(imageBitmap);
    }

    @OnClick(R.id.buttonProcess)
    public void buttonProcessClick() {
        seekBarThreshold1.setEnabled(true);
        seekBarThreshold2.setEnabled(true);

        processFilter();
    }

    private void processFilter() {
        originalMat = new Mat();
        bmp32 = imageBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, originalMat);

        grayMat = new Mat(originalMat.size(), originalMat.type());
        Imgproc.cvtColor(originalMat, grayMat, Imgproc.COLOR_RGBA2GRAY);


        resultMat = new Mat(grayMat.size(), grayMat.type());

        Imgproc.Canny(grayMat, resultMat, valueThreshold1, valueThreshold2);

        resultBitmap = Bitmap.createBitmap(resultMat.cols(), resultMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(resultMat, resultBitmap);
        imageViewHasil.setImageBitmap(resultBitmap);

    }



}
